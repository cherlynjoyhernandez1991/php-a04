<?php require_once 'code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>OOP Activity 2 (Access Modifiers and Encapsulation)</title>
</head>
<body>
	<h2>Building</h2>
	<p>The name of the building is <?= $building->getName(); ?>.</p>
	<p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?>.</p>
	<p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?>.</p>

	<!-- Change name value -->
	<?php $building->setProperties('Caswynn Complex', 8, 'Timog Avenue, Quezon City, Philippines'); ?>
	<p>The name of the building has been changed to <?= $building->getName(); ?>.</p>

	<h2>Condominium</h2>
	<p>The name of the condominium is <?= $condominium->getName(); ?>.</p>
	<p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?>.</p>
	<p>The <?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?>.</p>

	<!-- Change name value -->
	<?php $condominium->setProperties('Enzo Tower', 5, 'Buendia Avenue, Makati City, Philippines'); ?>
	<p>The name of the condominium has been changed to <?= $condominium->getName(); ?>.</p>
</body>
</html>